QT       += core gui quick qml network positioning widgets quickcontrols2 multimedia concurrent
QT +=  webchannel multimediawidgets charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    car_status/ap3216c.cpp \
    car_status/icm20608.cpp \
    keyboard.cpp \
    main.cpp \
    music/songbutton.cpp \
    navigation/navigation_widget.cpp \
    network/client.cpp \
    view/car_sta_view.cpp \
    view/music_view.cpp \
    view/video_view.cpp \
    widget.cpp

HEADERS += \
    car_status/ap3216c.h \
    car_status/icm20608.h \
    keyboard.h \
    music/songbutton.h \
    navigation/navigation_widget.h \
    network/client.h \
    view/car_sta_view.h \
    view/music_view.h \
    view/video_view.h \
    widget.h

TRANSLATIONS += \
    IVIS_BaseOnArm_zh_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    tools/path_all.inc

RESOURCES += \
    rsc.qrc

FORMS += \
    view/car_sta_view.ui

