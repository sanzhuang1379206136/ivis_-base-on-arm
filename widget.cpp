#include "widget.h"

#include<QAction>
#include<QPainter>
#include<QString>
#include<QPushButton>
#include<QProcess>
#include<QDebug>
#include<QPainter>
#include<QTimer>
#include<QTime>
#include<QThread>
#include<QLabel>
#include "QDate"


#include "view/video_view.h"
#include "view/music_view.h"
#include "view/car_sta_view.h"
#include "navigation/navigation_widget.h"

navigation_widget *g_navigation_view = nullptr;
Car_sta_view *g_car_sta_view = nullptr;
VideoView *g_video_view = nullptr;
MusicView *g_music_view = nullptr;

QTimer * time1, *mouse_timer;

Main_View::Main_View(QWidget *parent)
    : QMainWindow(parent)
{
    //ui->setupUi(this);


    //this->setCursor(Qt::BlankCursor); //隐藏鼠标
    this->setFixedSize( 1020, 600);//设置界面固定大小

    this->setWindowTitle("车载系统");

    this->setStyleSheet("background-color: #7FD8E6; color: #fff;");

#if 0
    // 创建一个 QLabel 并加载背景图片
    QLabel *backgroundLabel = new QLabel(this);
    backgroundLabel->setGeometry(this->rect());
    QPixmap backgroundImage(":/view/main_view/main_background.jpg"); // 替换为您的背景图片路径
    QPixmap scaledImage = backgroundImage.scaled(this->size(), Qt::KeepAspectRatioByExpanding);
    backgroundLabel->setPixmap(scaledImage);
    backgroundLabel->raise();
#endif




    //给进程分配空间
    //mypro = new QProcess(this);

    //安装事件过滤器
    this->installEventFilter(this);

    //时间显示label
    QLabel *time_label = new QLabel(this);
    time_label->setGeometry(380,20,320,80);//设置坐标
    time_label->setStyleSheet("font-size:80px;color:white");//设置大小颜色

    //日期显示label
    QLabel *date_label = new QLabel(this);
    date_label->setGeometry(490,100,240,80);//设置坐标
    date_label->setStyleSheet("font-size:25px;color:white");//设置大小颜色

    //定时器刷新显示
    QTimer *timer = new QTimer(this);
    timer->start(200);

    connect(timer,&QTimer::timeout,[=](){
        QTime tim = QTime::currentTime();//获取当前时间
            time_label->setText(tim.toString());

        QDate date = QDate::currentDate();//获取当前日期
        QString date_msg = QString("%1-%2-%3").arg(date.year()).arg(date.month()).arg(date.day());
            date_label->setText(date_msg);

    });

    //定时器用来自动隐藏鼠标光标
    mouse_timer = new QTimer(this);

    connect(mouse_timer,&QTimer::timeout,[=](){
        this->setCursor(Qt::BlankCursor); //隐藏鼠标
    });


    myPushButton * video_button=new myPushButton(":/view/main_view/video.png","",400,300);
    video_button->setParent(this);
    video_button->move(60,300);
    //视频按钮按下处理
    connect(video_button,&myPushButton::clicked,[=](){
        video_button->zoom1();//弹跳
        video_button->zoom2();
        //this->hide();
        g_video_view->show();
        g_video_view->raise();

        });


    myPushButton * music_button=new myPushButton(":/view/main_view/music.png","",400,300);
    music_button->setParent(this);
    music_button->move(600,300);
    //视频按钮按下处理
    connect(music_button,&myPushButton::clicked,[=](){
        music_button->zoom1();//弹跳
        music_button->zoom2();
        //this->hide();
        g_music_view->show();
        g_music_view->raise();

        });



    myPushButton * navigation_button=new myPushButton(":/view/main_view/navigation.png","",300,300);
    navigation_button->setParent(this);
    navigation_button->move(50,0);
    //navigation_res  g_navigation_view
    connect(navigation_button,&myPushButton::clicked,[=](){
        navigation_button->zoom1();//弹跳
        navigation_button->zoom2();
        //this->hide();
        g_navigation_view->show();
        g_navigation_view->raise();

        });

    myPushButton * car_sta_button=new myPushButton(":/view/main_view/car_sta.jpg","",300,300);
    car_sta_button->setParent(this);
    car_sta_button->move(700,0);
    //navigation_res  g_navigation_view
    connect(car_sta_button,&myPushButton::clicked,[=](){
        car_sta_button->zoom1();//弹跳
        car_sta_button->zoom2();
        //this->hide();
        g_car_sta_view->show();
        g_car_sta_view->raise();

        });





#if 0
    //音乐按钮
    myPushButton * music_button=new myPushButton(":/pic/music.png","",230,230);
    time1= new QTimer(this);
    music_button->setParent(this);
    music_button->move(285,10);

    //音乐按钮按下处理
    connect(music_button,&myPushButton::clicked,[=](){
        music_button->zoom1();//弹跳
        music_button->zoom2();
        time1->start(500);
        connect(time1,&QTimer::timeout,[=](){
            time1->stop();
            mypro->close();
            mypro->start("./QMusicPlayer");});
        });

    //硬件数据按钮
    myPushButton * hardware_button=new myPushButton(":/pic/setting(1).png","",230,230);
    time1= new QTimer(this);
    hardware_button->setParent(this);
    hardware_button->move(285,230);

    //硬件数据按下处理
    connect(hardware_button,&myPushButton::clicked,[=](){
        hardware_button->zoom1();//弹跳
        hardware_button->zoom2();
        time1->start(500);
        connect(time1,&QTimer::timeout,[=](){
            time1->stop();
            mypro->close();
            mypro->start("./chart_temp");});
        });



    //视频按钮
    myPushButton * video_button=new myPushButton(":/pic/video.jpeg","",250,200);
    video_button->setParent(this);
    video_button->move(550,25);

    //视频按钮按下处理
    connect(video_button,&myPushButton::clicked,[=](){
        video_button->zoom1();//弹跳
        video_button->zoom2();
        time1->start(500);//定时500ms
        connect(time1,&QTimer::timeout,[=](){
            time1->stop(); //关闭定时器
            mypro->close();
            mypro->start("./Camera");   });
        });



    //地图按钮
    myPushButton * map_button=new myPushButton(":/pic/bofangqi.png","",250,200);
    map_button->setParent(this);
    map_button->move(550,255);

    //地图按钮按下处理
    connect(map_button,&myPushButton::clicked,[=](){
        map_button->zoom1();
        map_button->zoom2();
        time1->start(300);
        connect(time1,&QTimer::timeout,[=](){
            time1->stop();
            mypro->close();
            mypro->start("./QVideoPlayer");   });
    });

    //天气按钮
    myPushButton * weather_button=new myPushButton(":/pic/weather.jpg","",250,200);
    weather_button->setParent(this);
    weather_button->move(10,255);

    //天气按钮按下处理
    connect(weather_button,&myPushButton::clicked,[=](){
        weather_button->zoom1();//弹跳
        weather_button->zoom2();
        time1->start(300);




        });

    });
#endif
}

Main_View::~Main_View()
{
    delete this;
}




//事件过滤器
bool Main_View::eventFilter(QObject *watched, QEvent *event)
{

    if(watched == this ){

        switch (event->type()) {
        case QEvent::MouseButtonPress:
                 mouse_timer->start(10000);
                 this->setCursor(Qt::ArrowCursor);  //显示正常鼠标
            break;
        default:
            break;
        }
    }

    return QWidget::eventFilter(watched,event);//将事件传递给父类
}


myPushButton::myPushButton(QString normal_path,QString press_path,int pixwidth,int pixheight)
{
    this->normal_path=normal_path;
    this->press_path=press_path;

    QPixmap pix;
    bool ret = pix.load(this->normal_path);
    if(!ret)
    {
        qDebug()<<"图片加载失败";
        return ;

    }
    //设置图片固定大小
    this->setFixedSize(pixwidth,pixheight);
    //设置不规则图片样式
    this->setStyleSheet( "QPushButton{border:0px;}" );
    //设置图标
    this->setIcon(pix);
    //设置图标大小
    this->setIconSize(QSize(pixwidth,pixheight));

    this->setFocusPolicy(Qt::NoFocus);     //去除虚线边框

    animation = new QPropertyAnimation(this,"geometry");

}

void myPushButton::zoom1()
{
    //设置动画时间间隔
    animation->setDuration(200);
    //设置起始位置
    animation->setStartValue(QRect(this->x(),this->y()+10,this->width(),this->height()));
    //设置结束位置
    animation->setEndValue(QRect(this->x(),this->y(),this->width(),this->height()));
    //设置弹跳曲线
    animation->setEasingCurve(QEasingCurve::OutBounce);
    //执行动画
    animation->start();
}

void myPushButton::zoom2()
{
    //设置动画时间间隔
    animation->setDuration(200);
    //设置起始位置
    animation->setStartValue(QRect(this->x(),this->y(),this->width(),this->height()));
    //设置结束位置
    animation->setEndValue(QRect(this->x(),this->y()-10,this->width(),this->height()));
    //设置弹跳曲线
    animation->setEasingCurve(QEasingCurve::InElastic);
    //执行动画
    animation->start();
}

void myPushButton::mousePressEvent(QMouseEvent *e)//鼠标按下
{
    if(this->press_path != "")
    {
        QPixmap pix;
        bool ret = pix.load(this->press_path);
        if(!ret)
        {
            qDebug()<<"图片加载失败";
            return ;
        }
        //设置图片固定大小
        this->setFixedSize(pix.width(),pix.height());
        //设置不规则图片样式
        this->setStyleSheet("QPushButton{border:0px;}");
        //设置图标
        this->setIcon(pix);
        //设置图标大小
        this->setIconSize(QSize(pix.width(),pix.height()));
    }
    return QPushButton::mousePressEvent(e);

}

void myPushButton::mouseReleaseEvent(QMouseEvent *e)//鼠标释放
{
    if(this->press_path != "")
    {
        QPixmap pix;
        bool ret = pix.load(this->normal_path);
        if(!ret)
        {
            qDebug()<<"图片加载失败";
            return ;
        }
        //设置图片固定大小
        this->setFixedSize(pix.width(),pix.height());
        //设置不规则图片样式
        this->setStyleSheet("QPushButton{border:0px;}");
        //设置图标
        this->setIcon(pix);
        //设置图标大小
        this->setIconSize(QSize(pix.width(),pix.height()));

    }
    return QPushButton::mouseReleaseEvent(e);
}




