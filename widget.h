#ifndef WIDGET_H
#define WIDGET_H

#include <QMainWindow>
#include <QWidget>
#include <QPushButton>

#include <QPropertyAnimation>
#include <QString>
#include <QEvent>
#include <QMouseEvent>
#include "view/video_view.h"
#include "view/music_view.h"
#include "view/car_sta_view.h"
#include "navigation/navigation_widget.h"
extern VideoView *g_video_view;
extern MusicView *g_music_view;
extern navigation_widget *g_navigation_view;
extern Car_sta_view *g_car_sta_view;
class Main_View : public QMainWindow
{
    Q_OBJECT

public:
    Main_View(QWidget *parent = nullptr);
    ~Main_View();

private:

    bool eventFilter(QObject *watched, QEvent *event);
};

class myPushButton : public QPushButton
{
    Q_OBJECT
public:
    myPushButton(QString normal_path,QString press_path="",int pixwidth=10,int pixheight=10);
    void zoom1();
    void zoom2();
private:
    QString normal_path;
    QString press_path;
    QPropertyAnimation* animation;
protected:
    void mousePressEvent(QMouseEvent * e);
    void mouseReleaseEvent(QMouseEvent * e);
signals:

public slots:
};

#endif //

