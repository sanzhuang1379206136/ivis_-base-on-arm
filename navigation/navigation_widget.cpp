#include "navigation_widget.h"
#include "network/client.h"
#include "keyboard.h"
#include <QCoreApplication>
#include <QFileInfoList>
#include <QDir>
#include <QProcess>
#include <QApplication>
#include <QFile>
#include <QPixmap>
#include <QDebug>
#define WIDTH 820
#define HIGH 600

navigation_widget::navigation_widget(QWidget *parent) : QWidget(parent)
{
    this->setGeometry(0, 0,1020 , 600);
    this->setStyleSheet("background:white");
    //this->setAttribute(Qt::WA_TranslucentBackground);



    // 创建 QLabel，并设置图片作为背景
    this->label_map = new QLabel(this);
    label_map->setGeometry(0, 0, 820, 600); // x=100, y=100, 宽=200, 高=100
    qDebug()<<"awg";
    QPixmap *pix = new QPixmap("/home/ztc/output.jpg");
    QPixmap scaledPixmap = pix->scaled(label_map->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    label_map->setPixmap(scaledPixmap);
    //label_map->setMinimumSize(1, 1); // 保证 QLabel 小于图片大小时也能正确显示
    //label_map->raise();
    label_map->show();// 显示 Label
    qDebug()<<"avc";
    //reload watcher
    watcher.addPath("/home/ztc/output.jpg");
    connect(&watcher, &QFileSystemWatcher::fileChanged, [=]() {
        QPixmap pixmap2("/home/ztc/output.jpg");
        QPixmap scaledPixmap2 = pixmap2.scaled(label_map->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        label_map->setPixmap(scaledPixmap2);
    });




    //init lineedit begin and end;
    this->lineedit_begin_pos = new QLineEdit(this);
    this->lineedit_begin_pos->setGeometry(820,100,200,50);
    connect(lineedit_begin_pos, &QLineEdit::selectionChanged, [=]() {
        qDebug()<<"askldjlad";
        KeyBoard *tmp_keyboard1 = new KeyBoard(this,lineedit_begin_pos);
    });
    this->lineedit_end_pos = new QLineEdit(this);
    this->lineedit_end_pos->setGeometry(820,200,200,50);
    connect(lineedit_end_pos, &QLineEdit::selectionChanged, [=]() {
        qDebug()<<"selectionChanged";
        KeyBoard *tmp_keyboard2 = new KeyBoard(this,lineedit_end_pos);
    });




    //init query button
    button_query = new QPushButton(this);
    this->button_query->setGeometry(820,280,75,50);
    button_query->setText("OK");
    connect(button_query, &QPushButton::clicked, [=]() {
        protocol_data_prepare(1,this->lineedit_end_pos->text());
    });


    //exit box
    //this->setFocus();
    exit_button = new QPushButton(this);
    exit_button->setMinimumSize(50, 50);
    exit_button->setMaximumSize(50, 50);
    exit_button->move(980,0);
    exit_button->setStyleSheet("QPushButton{background:red}");
    connect(exit_button,&QPushButton::clicked,[=](){
        this->setVisible(false);
    });



}
void navigation_widget::on_exitbuttonClicked(){

    this->setVisible(false);

}
