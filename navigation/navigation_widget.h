#ifndef NAVIGATION_WIDGET_H
#define NAVIGATION_WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QFileSystemWatcher>
class navigation_widget : public QWidget
{
    Q_OBJECT
public:
    explicit navigation_widget(QWidget *parent = nullptr);
    QLabel* label_map;
    QFileSystemWatcher watcher;

    QPushButton *exit_button;

    QLineEdit *lineedit_begin_pos;
    QLineEdit *lineedit_end_pos;

    QPushButton * button_query;
    

signals:


public slots:
    void on_exitbuttonClicked();




};

#endif // NAVIGATION_WIDGET_H
