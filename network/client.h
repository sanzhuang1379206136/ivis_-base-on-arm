#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QDataStream>
#include <QIODevice>
#include <QThread>
#include <QString>
#define SERVER_IP "192.168.10.200"
#define SERVER_PORT 6666
class Client;
extern Client *g_client;
void protocol_data_prepare(int mode,QString str);
class Client : public QObject
{
    Q_OBJECT
public:
    QTcpSocket* m_tcpSocket;    //tcp客户端
    Client(QObject *parent = nullptr);
    bool getConnectStatus();//get connect status:true/false

    void protocol_parse(QByteArray* byteArray);

private slots:
    void connectToServer(); //connct
    void onReadMessage();   //处理服务端反馈的数据
    void onDisplayError(QAbstractSocket::SocketError e);    //打印错误信息
 #if 0
    void on_connectBtn_clicked();   //点击连接按钮响应信号的槽函数
    void on_sendBtn_clicked();  //点击发送按钮响应的槽函数
#endif
private:

    QTimer* timer;
    bool connect_status;
};

class ParseDataThread : public QThread
{
public:
    void run() override;
    QTcpSocket* m_tcpSocket;    //tcp客户端
    int end_flag;
};

#endif // CLIENT_H
