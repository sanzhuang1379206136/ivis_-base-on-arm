#include "client.h"
#include <QAbstractSocket>
#include <QByteArray>
#include <QString>
#include <QDataStream>
#include <QtConcurrent>
#include <QDebug>
#include <QFile>
Client *g_client = nullptr;
static QTcpSocket* g_m_tcpSocket;
ParseDataThread g_data_parse_thread;
QByteArray parse_data;
Client::Client(QObject *parent)
{
    m_tcpSocket=new QTcpSocket(this);
    g_m_tcpSocket = m_tcpSocket;
    g_client = this;

    g_data_parse_thread.end_flag = 1;

    timer = new QTimer();
    connect_status = false;

    //socket有数据来了，做处理 multi thread
    QObject::connect(this->m_tcpSocket, &QTcpSocket::readyRead, this, &Client::onReadMessage);

#if 0
    //socket有数据来了，做处理
    connect(m_tcpSocket,&QTcpSocket::readyRead,
            this,&Client::onReadMessage);
#endif
    connect(m_tcpSocket,SIGNAL(QAbstractSocket::SocketError),
            this,SLOT(onDisplayError(QAbstractSocket::SocketError)));

    //continue to connect to host
    connect(timer, &QTimer::timeout, this, &Client::connectToServer);


    m_tcpSocket->abort();
    //连接服务端
    m_tcpSocket->connectToHost(SERVER_IP,SERVER_PORT);
    timer->start(10000);//start timer to retry to connect;

}
void Client::connectToServer(){
    //if(!m_tcpSocket->isOpen()){


    //}
    if(m_tcpSocket->state()==QAbstractSocket::UnconnectedState){
        //connected
        m_tcpSocket->connectToHost(SERVER_IP,6666);
    }else{
        //unconnected
    }

}
bool Client::getConnectStatus(){
    return this->connect_status;
}
void Client::onReadMessage(){
    if(g_data_parse_thread.end_flag==1){
        g_data_parse_thread.end_flag=0;
        g_data_parse_thread.start(); // 启动线程
    }


}




void Client::protocol_parse(QByteArray* byteArray){
    quint32 intValue;
    // 使用 QDataStream 对象读取 QByteArray
    //QDataStream dataStream(byteArray, QIODevice::ReadOnly);
    //dataStream.skipRawData(4); // 跳过前4个字节
    //dataStream >> intValue;
    //qDebug() << "Parsed length value: " << dataStream.size()<<endl;

    QByteArray newData = byteArray->mid(21);

    QFile pngFile("/home/ztc/output.png");
    if (!pngFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Failed to create output file.";
        return;
    }
    pngFile.write(newData);
    pngFile.close();

}
void Client::onDisplayError(QAbstractSocket::SocketError e){


    qDebug()<<"SocketErrorx:"<<e<<endl
           <<m_tcpSocket->errorString()<<endl;

}
void ParseDataThread::run(){
    qDebug() << "ParseDataThread started";

    qDebug() << "onReadMessage!" <<endl;
    static QByteArray data;
    parse_data.clear();
    int i=0;
    parse_data.append(g_client->m_tcpSocket->readAll());
#if 0
    while (g_client->m_tcpSocket->waitForReadyRead())
    {

        i++;
        if(i%10==0){
            qDebug() <<i<<":"<<parse_data.size()<<endl;

        }
        parse_data.append(g_client->m_tcpSocket->readAll());
    }
#endif
    qDebug() << "onReadMessage length value: ";
    qDebug() << "onReadMessage length value: " << parse_data.size()<<endl;

//=============================
    quint32 intValue;
    // 使用 QDataStream 对象读取 QByteArray
    //QDataStream dataStream(byteArray, QIODevice::ReadOnly);
    //dataStream.skipRawData(4); // 跳过前4个字节
    //dataStream >> intValue;
    //qDebug() << "Parsed length value: " << dataStream.size()<<endl;

    QByteArray newData = parse_data.mid(21);

    QFile pngFile("/home/ztc/output.jpg");

    if (!pngFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Failed to create output file.";
        return;
    }

    pngFile.write(newData);
    pngFile.close();
//================================

    //g_client->m_tcpSocket->write("rev ok",10);
    this->end_flag=1;
}



void protocol_data_prepare(int mode,QString str){
/*
    """16 bit/CID"""
    tmp_ivi_cid = [0,0]
    ivi_cid = bytes(tmp_ivi_cid)
    self.message_send += ivi_cid

    """8 bit/MODE"""
    tmp_ivi_mode = [0]
    ivi_mode = bytes(tmp_ivi_mode)
    self.message_send += ivi_mode

    """16 bit/MODE"""
    tmp_ivi_submode = [0,0]
    ivi_submode = bytes(tmp_ivi_submode)
    self.message_send += ivi_submode

    """128 bit/R2INFO"""
    tmp_ivi_r2info = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    ivi_r2info = bytes(tmp_ivi_r2info)
    self.message_send += ivi_r2info
*/

    QByteArray byteArray;
    QDataStream data(&byteArray, QIODevice::WriteOnly);

    // 写入16 bit/CID
    data << static_cast<quint16>(0);

    // 写入8 bit/MODE
    data << static_cast<quint8>(0);

    // 写入16 bit/MODE
    data << static_cast<quint16>(mode);

    data << str.toUtf8(); // 将字符串转换为UTF-8格式并写入流中

    qDebug()<<QByteArray::fromBase64(byteArray.toBase64());

    g_client->m_tcpSocket->write(byteArray);

}


