#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include "keyboard.h"


KeyBoard::KeyBoard(QWidget *parent,QLineEdit* lineEdit2){
    // 创建文本框

    this->lineEdit = new QLineEdit(this);
    lineEdit->text() = lineEdit2->text();
    this->lineEdit->setReadOnly(true);

    // 创建字母按钮
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *layout = nullptr;

    const QString buttons = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890";
    const QString buttons2 = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (int i = 0; i < buttons.size(); ++i) {
        if (i % 10 == 0) {
            layout = new QHBoxLayout();
            mainLayout->addLayout(layout);
        }

        QPushButton *button = new QPushButton(QString(buttons.at(i)), this);
        button->setFixedSize(40, 40);
        QObject::connect(button, &QPushButton::clicked, [=]() {
            lineEdit->insert(button->text());
        });
        layout->addWidget(button);
    }

    // 添加Backspace按钮和退出按钮
    QPushButton *backspaceButton = new QPushButton("⇦", this);
    backspaceButton->setFixedSize(40, 40);
    QObject::connect(backspaceButton, &QPushButton::clicked, [=]() {
        lineEdit->backspace();
    });

    QPushButton *exitButton = new QPushButton("Exit", this);
    exitButton->setFixedSize(40, 40);
    QObject::connect(exitButton, &QPushButton::clicked, [=]() {
        lineEdit2->setText(lineEdit->text());
        this->close();
    });

    // 设置虚拟键盘布局
    mainLayout->addWidget(backspaceButton);
    mainLayout->addWidget(exitButton);
    setLayout(mainLayout);

    this->setGeometry(0,300,1060,300);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->show();

}

