
#include <QApplication>
#include <QtWidgets>
#include <QtConcurrent>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLineEdit>


#include "navigation/navigation_widget.h"
#include "network/client.h"
#include "view/music_view.h"
#include "view/video_view.h"
#include "view/car_sta_view.h"
#include "keyboard.h"
#include "widget.h"


int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

#if 0
    QApplication a(argc, argv);

    Car_sta_view *view = new Car_sta_view();
    view->show();

    //navigation_widget *navigation_view = new navigation_widget();
    //navigation_view->show();

    return a.exec();


#elif 0
    QApplication app(argc, argv);

    QWidget wid;
    QLineEdit x;
    // 创建主窗口
    KeyBoard keyboardWindow(&wid,&x);
    keyboardWindow.show();

    return app.exec();
#else
    QApplication a(argc, argv);

    //Widget w;
    //w.show();
    VideoView *video_view = new VideoView();
    g_video_view = video_view;

    MusicView *music_view = new MusicView();
    g_music_view = music_view;

    navigation_widget* navigation_view = new navigation_widget();
    g_navigation_view = navigation_view;

    Car_sta_view* car_sta_view = new Car_sta_view();
    g_car_sta_view = car_sta_view;

    Client *client = new Client();
    g_client = client;

    //g_navigation_view->show();

    Main_View x;
    x.show();
    return a.exec();
#endif
}
