#ifndef SONGBUTTON_H
#define SONGBUTTON_H

#include <QObject>
#include <QPushButton>
#include <QLabel>
#include <QString>

class StatusPic;
class SongButton : public QPushButton
{
    Q_OBJECT
public:
    explicit SongButton(QPushButton* parent = nullptr);


    QLabel *title;
    QLabel *author;
    QLabel *time;
    StatusPic *status;

    void init_song_info(QString* m_title=nullptr,
                        QString* m_author=nullptr,
                        QString* m_time=nullptr);

    QString get_title(void);
    QString get_author(void);
    QString get_time(void);

    void init_component(void);

private:
    QString m_title ,m_author, m_time;
    //QUrl m_path;//picture path
};
class StatusPic : public QWidget
{
    Q_OBJECT
public:


};

#endif // SONGBUTTON_H
